This is a crypto-dashboard I built using the python-binance api. My interest 
was personal as I am invested in crypto and track the market. Also, at some point
of time I would like to implement algorithms for trading using technical indicators
and this is the first step in that direction. 

I have used the bokeh library before for plotting but this is also my first bokeh app.
I got to explore to experiment with live streaming data from binance and make
some interesting plots and charts.

Things to do:

- [x] BTC/ETH/LTC
- [x] Live streaming price data
- [x] Historical data (since 2017 or date since first introduced on binance)
- [x] EMA, MA, MACD bands
- [ ] Bollinder bands for historical data
- [x] Make the graphs update at the same time
- [x] Current market data
- [x] Current order book data
- [x] Add currencies I own!
- [x] Deploy on heroku
- [ ] If possible make initial calculations fast!
- [x] Fix datetime issues and Clean up logging code
