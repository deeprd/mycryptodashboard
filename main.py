import datetime
from bokeh.models import ColumnDataSource, HoverTool, SaveTool, DatetimeTickFormatter
from bokeh.models.widgets import Dropdown, RadioButtonGroup, PreText, Div
from bokeh.plotting import curdoc, Figure
from bokeh.layouts import row, column, widgetbox, layout
from binance.client import Client
from math import pi
import numpy as np
import pandas as pd

SYM = "BTCUSDT"
window = 50
data = ColumnDataSource(dict(
    time=[], low=[], high=[], opn=[], close=[], color = [],
    ma12=[], ma26 = [] ,macd = [], macd9=[], macdh=[], ema12 =[], ema26=[]
    
))

historical_data = ColumnDataSource(dict(
    time=[], low=[], high=[], opn=[], close=[], color =[], average = []))

client = Client("", "")


def get_last_data():
  
    """
    A typical response for a 1 limit candlestick data from binance
        [
        [
        1499040000000,      // Open time
        "0.01634790",       // Open
        "0.80000000",       // High
        "0.01575800",       // Low
        "0.01577100",       // Close
        "148976.11427815",  // Volume
        1499644799999,      // Close time
        "2434.19055334",    // Quote asset volume
        308,                // Number of trades
        "1756.87402397",    // Taker buy base asset volume
        "28.46694368",      // Taker buy quote asset volume
        "17928899.62484339" // Ignore.
    ]
    ]
    """
    candles = client.get_klines(symbol=SYM, interval='1m', limit=1)    
    time, opn, high, low, close = datetime.datetime.fromtimestamp(candles[0][6]/1e3), candles[0][1], candles[0][2], candles[0][3], candles[0][4]    
    return  time.replace(microsecond=0) , float(opn), float(high), float(low), float(close)

def update_symbol(attr, old, new):    
    global SYM    
    if radiobuttongroup.active == 0:
        SYM = 'BTCUSDT'
    elif radiobuttongroup.active == 1:
        SYM = 'ETHUSDT'
    elif radiobuttongroup.active == 2:
        SYM = 'LTCUSDT'
    elif radiobuttongroup.active == 3:
        SYM = 'AEETH'
    elif radiobuttongroup.active == 4:
        SYM = 'GNTETH'
    elif radiobuttongroup.active == 5:
        SYM = 'PIVXETH'
    elif radiobuttongroup.active == 6:
        SYM = 'NEOUSDT'      
    update_data_once()
    
def ema(data = data.data['close'], period=26):
    #print('EMA: ',type(pd.Series(data).ewm( span=period).mean()), len(pd.Series(data).ewm( span=period).mean()))
    return pd.Series(data).ewm( span=period).mean()



def ma(data = data.data['close'], ticks=50):        
    #print('MA: ', type(np.convolve(data, np.ones(ticks)/ticks, mode='valid')), len(np.convolve(data, np.ones(ticks)/ticks, mode='valid')) )
    return np.convolve(data, np.ones(ticks)/ticks, mode='valid')
    

def helper_once_ohlc():
    candles = client.get_klines(symbol=SYM, interval='1m', limit=100)
    time = [datetime.datetime.fromtimestamp(i[6]/1e3).replace(microsecond=0) for i in candles]
    opn = [float(i[1]) for i in candles]
    high = [float(i[2]) for i in candles]
    low = [float(i[3]) for i in candles]
    close = [float(i[4]) for i in candles]
    color = [ 'green' if values[0] < close[1] else 'red' for values in zip(opn, close)]    
    new_data = dict(
    time=time[50:],
    opn=opn[50:],
    high=high[50:],
    low=low[50:],
    close=close,    
    color = color[50:]
    )    
    ma12 = ma(new_data['close'], 12 )
    ma26 = ma(new_data['close'], 26 )
    ema12 = ema(new_data['close'], 12)
    ema26 = ema(new_data['close'], 26)    
    macd = ema12[-50:] - ema26[-50:]
    new_data['macd'] = list(macd)
    macd9 = ema(new_data['macd'], 9)    
    macdh = macd.values - macd9.values
    new_data['close'] = close[50:]
    new_data['ma12'] = list(ma12[-50:])
    new_data['ma26'] = list(ma26[-50:])
    new_data['ema12'] = list(ema12[-50:])
    new_data['ema26'] = list(ema26[-50:])
    new_data['macd'] = list(macd[-50:])
    new_data['macd9'] = list(macd9[-50:])
    new_data['macdh'] = list(macdh[-50:])        
    return new_data


def helper_once_historical():
    historical_candles = client.get_historical_klines(symbol=SYM, interval= Client.KLINE_INTERVAL_1WEEK, start_str="1 Jan, 2017")
    time = [datetime.datetime.fromtimestamp(i[6]/1e3).replace(microsecond=0) for i in historical_candles]
    opn = [float(i[1]) for i in historical_candles]
    high = [float(i[2]) for i in historical_candles]
    low = [float(i[3]) for i in historical_candles]
    close = [float(i[4]) for i in historical_candles]
    color = [ 'green' if values[0] < close[1] else 'red' for values in zip(opn, close)]    
    average = [ (o+h+l+c)/4 for o,h,l,c in zip(opn, high, low, close)]
    new_historical_data = dict(
    time=time,
    opn=opn,
    high=high,
    low=low,
    close=close,
    average = average,
    color = color
    )
    return new_historical_data


def market_data_stats():
    ticker_data = client.get_ticker(symbol=SYM)
    not_needed_list = ['weightedtAvgPrice', 'quoteVolume', 'firstId', 'lastId']
    string = '<ul>'
    for key, value in ticker_data.items():
        if key not in not_needed_list:
            string += '<li>' + key + ': ' + str(value) + '</li>'
    string += '</ul>'
    return string

def update_data_once():
    new_data = helper_once_ohlc()
    temp_hist_data = helper_once_historical()    
    data.stream(new_data, 50)       
    historical_data.data = temp_hist_data
    notifications.text = market_data_stats()     


def update_data():
    print('I am in update data')   
    time, opn, high, low, close=get_last_data()        
    if close > opn:
        color = 'green'
    else:
        color = 'red'
    new_data = dict(
    time=[time],
    opn=[opn],
    high=[high],
    low=[low],
    color = [color],
    close= data.data['close'] + [close]  
    )
    ma12 = ma(new_data['close'], 12)
    ma26 = ma(new_data['close'], 26 )
    ema12 = ema(new_data['close'], 12)
    ema26 = ema(new_data['close'], 26)    
    macd = ema12[-50:] - ema26[-50:]
    new_data['macd'] = list(macd)   
    macd9 = ema(new_data['macd'], 9)  
    macdh = macd.values - macd9.values    
    new_data['ma12'] = [ma12.tolist()[-1]]
    new_data['ma26'] = [ma26.tolist()[-1]]
    new_data['ema12'] = [ema12.tolist()[-1]]
    new_data['ema26'] = [ema26.tolist()[-1]]
    new_data['macd'] = [macd.tolist()[-1]]
    new_data['macd9'] = [macd9.tolist()[-1]]
    new_data['macdh'] = [macdh.tolist()[-1]]
    new_data['close'] = [close]
    print(new_data['ema26'])    
    for key, value in new_data.items():
        print(key, len(value))
    data.stream(new_data, 50)
    notifications.text = market_data_stats()     



price_plot = Figure(plot_height=400, plot_width = 900,tools="xpan,xwheel_zoom,xbox_zoom,reset", y_axis_location="right", title="Live OHLC data")
price_plot.x_range.follow = "end"
price_plot.x_range.range_padding = 0.01
price_plot.line(source=data, x='time', y='ema12', color = 'purple', legend='12 period ema')
price_plot.line(source=data, x='time', y='ema26', color = 'black', legend='26 period ema')
price_plot.xaxis.formatter=DatetimeTickFormatter(
        
    minutes=["%I:%M %p"],
    
    )
price_plot.xaxis.major_label_orientation = pi/4
price_plot.segment(x0='time', y0='low', x1='time', y1='high', color = 'black' ,line_width=2, source=data)
price_plot.segment(x0='time', y0='opn', x1='time', y1='close', color = 'color' ,line_width=8, source=data)


macd_plot = Figure(plot_height=400, plot_width = 900, x_range=price_plot.x_range, tools="xpan,xwheel_zoom,xbox_zoom,reset", y_axis_location="right", title="Live MACD plot")
macd_plot.line(x='time', y='macd', color='blue', source=data, legend='MACD')
macd_plot.line(x='time', y='macd9', color='red', source=data, legend='MACD signal')
macd_plot.segment(x0='time', y0=0, x1='time', y1='macdh', line_width=6, color='black', alpha=0.5, source=data)
macd_plot.xaxis.formatter=DatetimeTickFormatter(
        
    minutes=["%I:%M %p"],
    
    )
macd_plot.xaxis.major_label_orientation = pi/4


historical_plot = Figure(plot_height=400, plot_width = 900, tools="xpan,xwheel_zoom,xbox_zoom,reset", y_axis_location="right", title="Historical data")
historical_plot.line(x='time', y='average', color = 'blue', source=historical_data)
historical_plot.segment(x0='time', y0='low', x1='time', y1='high', color = 'black' ,line_width=1, source=historical_data)
historical_plot.segment(x0='time', y0='opn', x1='time', y1='close', color = 'color' ,line_width=3, source=historical_data)
historical_plot.xaxis.formatter=DatetimeTickFormatter(
    
    years=["%m/%d/%y"],
    )
historical_plot.xaxis.major_label_orientation = pi/4





radiobuttongroup=RadioButtonGroup(labels=['BTC-USDT', 'ETH-USDT', 'LTC-USDT', 'AE-ETH', 'GNT-ETH', 'PIVX-ETH', 'NEO-USDT'], active=0)
radiobuttongroup.on_change('active', update_symbol)
notifications = Div(text=market_data_stats())

controls = widgetbox(radiobuttongroup)
curdoc().add_root(column(controls, (row(column(price_plot, macd_plot), column(historical_plot, notifications)))))
curdoc().add_timeout_callback(update_data_once, 2000)
curdoc().add_periodic_callback(update_data, 60000) #60 sec per min * 1000 milliseconds per second
curdoc().title = 'Crypto'